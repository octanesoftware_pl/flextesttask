# Flexpedia Invoices Test Task

One Paragraph of project description goes here

## Getting Started

Task has been made without using any 3rd party frameworks as guided.
To avoid any problems while running code by supervisor, 
PHP 7.3 used as version wasn't specified (array_key_first for PHP 7.3 used).

Applications contains:
1. Listing invoices from attached DB
2. Exporting transactions from pagination
3. Exporting customer report
4. Feature to change payment status

### Prerequisites

To locally run app, apache has to be running. Go to guide:

```
https://www.wikihow.com/Install-XAMPP-for-Windows
```

### Installing

If apache has been installed and running, copy to your htdocs folder with project. Example:

```
flexpedia/
```

Copied to:

```
/opt/lampp/htdocs/flexpedia/
```

Which allow to reach website under:

```
http://localhost/flexpedia/
```

Properly installed app should show main page. 
If not - something went wrong while installing xampp or other.
Main page will show:

```
Flexpedia Invoices Test Task
SHOW INVOICES LIST

```

## Going to the list

To be able to see anything on the list, DB should be imported.
in `config.php` file settings to local database are defined.

Go to your (for example) PHPMyAdmin and import attached database into local DB named as set in config.
In our case database name is `flexpedia` which after importing contains two tables `invoices` and `invoice_items`.

Click on "Show invoices list" on main page or go to:
```
http://localhost/flexpedia/list.php
```

### Exporting

Top buttons on invoices list are meant to export as told.

```
Customer report & Export results
```
One will export all data, but the other only items show by pagination

### Status change

Among possibility to show details on each item (+), download details - you can also change payment status by clicking check/times button on each item on list.

## Built With

* [PDO](https://www.php.net/manual/en/book.pdo.php) - The PHP Data Objects
* [fPutCSV](https://www.php.net/manual/en/function.fputcsv.php)

## Versioning

1.0
## Authors

* **Joanna Kostek** - *for* - [Flexpedia](https://flexpedia.nl/)