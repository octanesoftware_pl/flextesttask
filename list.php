<?php

include './autoload.php';
include './functions.php';
include './model/Invoice.php';
include './model/InvoiceItem.php';

$db           = new Db();
$invoices     = new Invoice($db);
$invoiceItems = new InvoiceItem($db);

$limit = 5;
$chunk = !empty($_GET['p']) ? (int)$_GET['p'] - 1 : 0;
$page  = $chunk + 1;
$skip  = $limit * $chunk;

$isLastPage  = $page * $limit >= $invoices->count();
$isFirstPage = $page <= 1;

$invoicesPerPage = $invoices->findBy([], $skip, $limit);
$items = $invoiceItems->findByInvoicesIds(Invoice::getOnlyColumn('id', $invoicesPerPage));

$invItemsById = [];
foreach($items as $item){
    $invItemsById[$item['invoice_id']][] = $item;
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Flexpedia</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
    <link rel="stylesheet" href="assets/css/_all-skins.min.css">
    <link rel="stylesheet" href="assets/css/ionicons.min.css">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="position-ref full-height">

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
            </div>
        </div>
    <div class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Invoices</h3>
                        <hr>
                        <div class="box-tools">
                            <div class="input-group input-group-sm hidden-xs">
                                <a href="./index.php" class="btn btn-primary pull-right" role="button" title="Back to main"><i class="fa fa-chevron-left"></i></a>
                                <a href="./export.php?t=er&p=<?= $page ?>" class="btn btn-success pull-right margin-r-5" role="button" title="Export page to CSV"><i class="fa fa-file-archive-o"></i> Export results</a>
                                <a href="./export.php?t=cr" class="btn btn-success pull-right margin-r-5" role="button" title="Export page to CSV"><i class="fa fa-download"></i> Customer report</a>
                            </div>
                        </div>
                    </div>
                    <div class="box-header">
                        <div class="mailbox-controls">
                            <div class="pull-right">
                                <?= $skip + 1; ?>-<?=$limit * $page; ?>/<?= $invoices->count(); ?>
                                <div class="btn-group">
                                    <a href="./list.php<?= !$isFirstPage && $page - 1 > 1 ? '?p=' . ($page - 1) : '' ?>" type="button" class="btn btn-default btn-sm" <?= $isFirstPage ? 'disabled' : '' ?>><i class="fa fa-chevron-left"></i></a>
                                    <a href="./list.php<?= !$isLastPage ? '?p=' . ($page + 1) : '' ?>" type="button" class="btn btn-default btn-sm" <?= $isLastPage ? 'disabled' : '' ?>><i class="fa fa-chevron-right"></i></a>
                                </div>
                                <!-- /.btn-group -->
                            </div>
                            <!-- /.pull-right -->
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="main-table">
                            <tbody style="text-align: left;">
                            <tr>
                                <th>ID</th>
                                <th style="width: 45%;">Client</th>
                                <th>Amount Netto</th>
                                <th>VAT</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th>.</th>
                            </tr>
                            <?php
                                foreach($invoicesPerPage as $d):
                                    if(!Invoice::hasProperties($d)) continue;

                                    $hasItems = !empty($invItemsById[$d['id']]);
                                    $paid     = $d['invoice_status'] === 'paid'
                            ?>
                                <tr>
                                    <td><?= $d['id'] ?></td>
                                    <td><?= $d['client'] ?></td>
                                    <td><?= UX::amount($d['invoice_amount']) ?></td>
                                    <td><?= UX::prc($d['vat_rate']) ?></td>
                                    <td><span class="label label-<?= $paid ? 'success' : 'danger'; ?>"><?= ucfirst($d['invoice_status']); ?></span></td>
                                    <td><?= $d['invoice_date'] ?></td>
                                    <td>
                                        <?php if($hasItems): ?>
                                        <a class="btn btn-expand" data-toggle="collapse" href="#collapseData<?= $d['id'] ?>" role="button" aria-expanded="false" aria-controls="collapseData<?= $d['id'] ?>">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </a>
                                        <a class="btn btn-export" href="./export.php?t=cp&id=<?= $d['id'] ?>">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                        </a>
                                        <a class="btn btn-change" href="./status.php?c=<?= $paid ? 1 : 0 ?>&id=<?= $d['id'] ?>">
                                            <i class="fa fa-<?= $paid ? 'times' : 'check' ?>" title="<?= $paid ? 'Revert payment' : 'Mark as paid' ?>" aria-hidden="true"></i>
                                        </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr class="collapse" id="collapseData<?= $d['id'] ?>">
                                    <td class="card card-body" colspan="7">
                                        <?php if($hasItems): ?>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tbody>
                                                    <?php foreach($invItemsById[$d['id']] as $item): ?>
                                                    <tr>
                                                        <th style="width:50%"><?= $item['name'] ?></th>
                                                        <td><?= UX::amount($item['amount']) ?></td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="mailbox-controls">
                            <div class="pull-right">
                                <?= $skip + 1; ?>-<?=$limit * $page; ?>/<?= $invoices->count(); ?>
                                <div class="btn-group">
                                    <a href="./list.php<?= !$isFirstPage && $page - 1 > 1 ? '?p=' . ($page - 1) : '' ?>" type="button" class="btn btn-default btn-sm" <?= $isFirstPage ? 'disabled' : '' ?>><i class="fa fa-chevron-left"></i></a>
                                    <a href="./list.php<?= !$isLastPage ? '?p=' . ($page + 1) : '' ?>" type="button" class="btn btn-default btn-sm" <?= $isLastPage ? 'disabled' : '' ?>><i class="fa fa-chevron-right"></i></a>
                                </div>
                                <!-- /.btn-group -->
                            </div>
                            <!-- /.pull-right -->
                        </div>
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    </div>
</div>
</body>
<script>
    $(document).ready(function(){
        $('.btn-expand').on('shown.bs.collapse', function () {
            $("i.fa").removeClass("fa-minus").addClass("fa-plus");
        });

        $('.btn-expand').on('hidden.bs.collapse', function () {
            $("i.fa").removeClass("fa-plus").addClass("fa-minus");
        });
    })
</script>
</html>



