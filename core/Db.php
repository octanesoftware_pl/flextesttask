<?php

/**
 * Class Db
 * @property array _errors
 * @property string _status
 * @property PDO _connection
 */
class Db
{
    private $_errors;
    private $_status;
    private $_connection;

    /**
     * Db constructor.
     */
    public function __construct()
    {
        $config = include(__DIR__ . '/../config.php');

        try {
            $this->_connection = new PDO("mysql:host={$config['db_servername']};dbname={$config['db_dbname']}", $config['db_username'], $config['db_password']);
            $this->_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            if(!$this->_connection instanceof PDO) throw new Exception("Not able to reach PDO");
        } catch(PDOException $e) {
            $this->_addErr("Connection failed: " . $e->getMessage());
            $this->_setStatus('not connected');
        } catch(Exception $e) {
            $this->_addErr("Connection failed: " . $e->getMessage());
        }
        $this->_setStatus('connected');
    }

    /**
     *
     */
    public function __destruct()
    {
        $this->_connection = null;
    }


    /**
     * @param string $msg
     */
    private function _addErr($msg = '')
    {
        $this->_errors[] = $msg;
    }

    /**
     * @param string $status
     */
    private function _setStatus($status = '')
    {
        $this->_status = $status;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->_errors;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->_status;
    }

    /**
     * @return object PDO
     */
    public function connection()
    {
        return $this->_connection;
    }

    /**
     * @param $table
     * @param array $columns
     * @param null $condition
     * @param array $bindParams
     * @param int $skip
     * @param int $limit
     * @return array
     */
    public function select($table, $columns = [], $skip = 0, $limit = 1000, $condition = null, $bindParams = [])
    {
        $query = "SELECT ";
        if(empty($columns)){
            $query .= "* ";
        }else{
            foreach($columns as $column){
                $query .= "$column, ";
            }
            $query = rtrim($query, ", ") . " ";
        }

        $query .= "FROM `$table` ";
        if(!empty($condition)){
            $query .= "WHERE $condition ";
        }
        $query .= "LIMIT $skip, $limit;";

        if(!empty($bindParams)){
            $q = $this->_connection->prepare($query);
            foreach($bindParams as $paramName => $value){ //PDO::PARAM_STR
                $type = PDO::PARAM_STR;
                $q->bindParam(":$paramName", $value, $type, 12);
            }
        }else{
            $q = $this->_connection->query($query);
        }
        $rows = $q->fetchAll(PDO::FETCH_ASSOC);
        $q->closeCursor();

        return $rows;
    }

    /**
     * @param $table
     * @param array $columns
     * @param null $condition
     * @param array $bindParams
     * @return array
     */
    public function selectOne($table, $columns = [], $condition = null, $bindParams = [])
    {
        return $this->select($table, $columns, 0, 1, $condition, $bindParams);
    }

    /**
     * @param $query
     * @param string $method
     * @return mixed
     */
    public function query($query = null, $method = 'fetchAll')
    {
        if(empty($query)) return [];

        $q = $this->_connection->query($query);
        $result = $q->{$method}(PDO::FETCH_ASSOC);
        $q->closeCursor();

        return $result;
    }

    /**
     * @param null $query
     * @return bool|int
     */
    public function execute($query = null)
    {
        if(empty($query)) return false;

        return $this->_connection->exec($query);
    }

    /**
     * @param $table
     * @return int
     */
    public function count($table)
    {
        $query = "SELECT COUNT(id) as count ";
        $query .= "FROM `$table` ";

        $q = $this->_connection->query($query);
        if(!empty($bindParams)){
            foreach($bindParams as $paramName => $type){ //PDO::PARAM_STR
                $q->bindParam(":$paramName", $$paramName, $type, 12);
            }
        }

        $rows = $q->fetch(PDO::FETCH_ASSOC);
        $q->closeCursor();

        return isset($rows['count']) ? (int)$rows['count'] : 0;
    }
}