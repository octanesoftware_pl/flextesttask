<?php

/**
 * Class Model
 * @property string _table
 * @property Db _db
 */
class Model
{
    protected $_table = '';

    const LIMIT = 500;

    /**
     * Model constructor.
     * @param $db
     */
    public function __construct($db)
    {
        $this->_db     = $db;
        $this->setTable(strtolower(self::class) . "s");
    }

    public static function rules()
    {
        return ['id'];
    }

    /**
     * @param array $array
     * @return bool
     */
    public static function hasProperties($array = [])
    {
        foreach(static::rules() as $propertyName => $type){
            if(!isset($array[$propertyName])) return false;
        }

        return true;
    }

    /**
     * @param $table
     */
    protected function setTable($table)
    {
        $this->_table = $table;
    }

    /**
     * @param array $columns
     * @return array
     */
    public function findAll($columns = [])
    {
        return $this->_db->select($this->_table, $columns, 0, self::LIMIT, null, []);
    }

    /**
     * @param $columns
     * @param int $skip
     * @param int $limit
     * @param null $condition
     * @param array $bind
     * @return array
     */
    public function findBy($columns, $skip = 0, $limit = self::LIMIT, $condition = null, $bind = [])
    {
        return $this->_db->select($this->_table, $columns, $skip, $limit, $condition, $bind);
    }

    /**
     * @param null $id
     * @return array
     */
    public function findOne($id = null)
    {
        return $this->_db->selectOne($this->_table, [], 'id = :id', ['id' => $id]);
    }

    /**
     * @param null $id
     * @return array
     */
    public function findById($id = null)
    {
        return $this->_db->selectOne($this->_table, [], 'id = :id', ['id' => $id]);
    }

    /**
     * @return mixed
     */
    public function count()
    {
        return $this->_db->count($this->_table);
    }

    /**
     * @param null $query
     * @param string $method
     * @return array|mixed
     */
    public function query($query = null, $method = 'fetchAll')
    {
       return !empty($query) ? $this->_db->query($query, $method) : [];
    }

    /**
     * @param null $query
     * @return bool
     */
    public function execute($query = null)
    {
       return !empty($query) ? $this->_db->execute($query) : false;
    }
}