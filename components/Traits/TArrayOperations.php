<?php

/**
 * Trait TArrayOperations
 */
trait TArrayOperations
{
    /**
     * @param string $column
     * @param array $array
     * @return array
     */
    public static function getOnlyColumn($column = '', array $array = [])
    {
        $result = [];
        if(!empty($array) && !empty($column)){
            foreach($array as $a){
                if(isset($a[$column])) $result[] = $a[$column];
            }
        }

        return $result;
    }
}