<?php

/**
 * Class UX
 */
class UX
{
    const CURRENCY_STR   = '€';
    const CURRENCY_PLACE = 0;

    /**
     * @param int $amount
     * @return string
     */
    public static function prc($amount = 0)
    {
        return (int)$amount . '%';
    }

    /**
     * @param float $number
     * @return string
     */
    public static function amount($number = 0.00)
    {
        return self::CURRENCY_PLACE === 0
            ? self::CURRENCY_STR . number_format($number, 2, '.', '')
            : number_format($number, 2, '.', '') . self::CURRENCY_STR;
    }
}