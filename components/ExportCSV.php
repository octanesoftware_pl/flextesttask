<?php

/**
 * Class ExportCSV
 * @property string _type
 * @property int    _page
 * @property array  _data
 * @property string _fileName
 */
class ExportCSV
{
    const EXPORT_RESULTS  = 'er';
    const CUSTOMER_REPORT = 'cr';
    const COMPANY_REPORT  = 'cp';

    private $_type;
    private $_page;
    private $_data;
    private $_fileName;

    /**
     * ExportCSV constructor.
     * @param string $type customer report / export results
     */
    public function __construct($type = self::CUSTOMER_REPORT)
    {
        $this->_type = $type;
    }

    /**
     * @param int $page
     */
    public function setDataPart($page = 1)
    {
        $this->_page = $page;
    }

    /**
     * @param Invoice|InvoiceItem $model
     * @param int $page
     * @param null $singleInvoiceId
     */
    public function prepareData($model, $page = 1, $singleInvoiceId = null)
    {
        $fileName = 'export_' . time();
        $this->_data = $columns = [];
        if($this->_type == self::EXPORT_RESULTS) {
            $limit = 5;
            $skip = ($page - 1) * $limit;
            $columns = ['id', 'client', 'invoice_amount'];

            $this->_data = $model->findBy($columns, $skip, $limit);
            $fileName = 'export_' . $this->_type . '_' . time();
        }else if($this->_type == self::CUSTOMER_REPORT){
            $limit   = 1000;
            $skip    = 0;

            $query = "SELECT ";
            //$query .= "client, invoice_amount as net_amount, invoice_amount_plus_vat as gross_amount";
            $query .= "client, invoice_amount_plus_vat as gross_amount";
            $query .= ", ";

            $query .= "CASE invoice_status ";
            $query .= "WHEN 'paid' THEN invoice_amount_plus_vat ";
            $query .= "WHEN 'unpaid' THEN 0 ";
            $query .= "END as paid_amount ";
            $query .= ", ";

            $query .= "CASE invoice_status ";
            $query .= "WHEN 'paid' THEN 0 ";
            $query .= "WHEN 'unpaid' THEN invoice_amount_plus_vat ";
            $query .= "END as outstanding_amount ";

            $query .= "FROM `invoices` ";
            $query .= "LIMIT {$skip}, {$limit};";

            $this->_data = $model->query($query);
            $fileName = 'export_' . $this->_type . '_' . time();
        }else if($this->_type == self::COMPANY_REPORT && !empty($singleInvoiceId)){
            $limit   = 1000;
            $skip    = 0;

            $query = "SELECT ";
            $query .= "i.client, t.name as item_name, t.amount as item_price, ";
            $query .= "i.invoice_status, i.invoice_date, i.invoice_amount, i.vat_rate ";

            $query .= "FROM `invoice_items` as t ";
            $query .= "LEFT JOIN invoices as i ON i.id = t.invoice_id ";
            $query .= "WHERE t.`invoice_id` = {$singleInvoiceId} LIMIT {$skip}, {$limit};";

            $this->_data = $model->query($query);
            $fileName = 'export_' . $this->_type . '_' . $singleInvoiceId . '_' . time();
        }

        $this->_fileName = $fileName;
    }

    /**
     * @param bool $dumpOnly
     */
    public function run($dumpOnly = false)
    {
        $output = fopen("php://output", "w");

        try{
            if(empty($this->_data)) throw new Exception("Empty data");
            $lvlOneKey = array_key_first($this->_data);

            //putting
            $keys = array_keys($this->_data[$lvlOneKey]);
            fputcsv($output, $keys);

            foreach($this->_data as $arr){
                fputcsv($output, $arr);
            }
        } catch(Exception $e){

        }
        fclose($output);

        if(!$dumpOnly){
            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename={$this->_fileName}.csv");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
        }else{
            die();
        }
    }
}