<?php

/**
 * Class HandleStatus
 * @property $_status
 * @property $_itemId
 */
class HandleStatus
{
    private $_itemId = null;
    private $_status = null;

    /**
     * HandleStatus constructor.
     * @param $itemId
     * @param null $currentStatus
     */
    public function __construct($itemId, $currentStatus = null)
    {
        $this->setItemId($itemId);
        $this->setStatus($currentStatus);

        return $this;
    }

    /**
     * @param null $itemId
     * @return HandleStatus
     */
    public function setItemId($itemId)
    {
        $this->_itemId = $itemId;
        return $this;
    }

    /**
     * @return null
     */
    public function getItemId()
    {
        return $this->_itemId;
    }

    /**
     * @param null $status
     * @return HandleStatus
     */
    public function setStatus($status)
    {
        $this->_status = $status;
        return $this;
    }

    /**
     * @return null
     */
    public function getStatus()
    {
        return $this->_status;
    }

    /**
     * @param Invoice|InvoiceItem $model
     * @return bool
     */
    public function change($model)
    {
        if($this->getStatus() === null) return false;
        $newStatus = $this->getStatus() === "unpaid" ? "paid" : "unpaid";
        $query = "UPDATE `invoices` SET `invoice_status` = '{$newStatus}' WHERE `invoices`.`id` = {$this->getItemId()};";
        $changed = $model->execute($query);

        return (bool)$changed;
    }

}