<?php

/**
 * Class Invoice
 * @property $_id
 * @property $_client
 * @property $_invoice_amount
 * @property $_invoice_amount_plus_vat
 * @property $_vat_rate
 * @property $_invoice_status
 * @property $_invoice_date
 * @property $_created_at
 */
class Invoice extends Model
{
    use TArrayOperations;

    private $_id;
    private $_client;
    private $_invoice_amount;
    private $_invoice_amount_plus_vat;
    private $_vat_rate;
    private $_invoice_status;
    private $_invoice_date;
    private $_created_at;

    /**
     * Invoice constructor.
     * @param $db
     */
    public function __construct($db)
    {
        parent::__construct($db);
        $this->setTable(strtolower(self::class) . "s");
    }

    /**
     * @return array
     */
    public static function rules()
    {
        return [
            'id'                      => 'integer',
            'client'                  => 'string',
            'invoice_amount'          => 'float',
            'invoice_amount_plus_vat' => 'float',
            'vat_rate'                => 'float',
            'invoice_status'          => 'string',
            'invoice_date'            => 'date',
            'created_at'              => 'date'
        ];
    }

    /**
     * @param array $columns
     * @return array
     */
    public function findAll($columns = [])
    {
        return parent::findAll($columns);
    }

    /**
     * @param array $a
     * @return bool
     */
    public static function hasProperties($a = [])
    {
        return parent::hasProperties($a);
    }

//    /**
//     * @param array $arrayWithValues
//     * @return $this
//     */
//    public function obj($arrayWithValues = [])
//    {
//        $properties = get_object_vars(new self(new Db()));
//
//        foreach($properties as $key => $property){
//            $arKey = ltrim($key, '_');
//            if(isset(self::rules()[$arKey])){
//                $this->{$key} = $arrayWithValues[$arKey];
//            }
//        }
//        return $this;
//    }
}