<?php

/**
 * Class InvoiceItem
 */
class InvoiceItem extends Model
{
    private $_id;
    private $_invoice_id;

    /**
     * Invoice constructor.
     * @param $db
     */
    public function __construct($db)
    {
        parent::__construct($db);
        //$this->setTable(strtolower(self::class) . "s");
        $this->setTable("invoice_items");
    }

    /**
     * @return array
     */
    public static function rules()
    {
        return [
            'id'         => 'integer',
            'invoice_id' => 'string',
            'name'       => 'string',
            'amount'     => 'float',
            'created_at' => 'date'
        ];
    }

    /**
     * @param array $columns
     * @return array
     */
    public function findAll($columns = [])
    {
        return parent::findAll($columns);
    }

    public function findByInvoicesIds($ids = [])
    {
        return $this->findBy([], 0, self::LIMIT, 'invoice_id IN (' . implode(", ", $ids) . ') ');
    }
}