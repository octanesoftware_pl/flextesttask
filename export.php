<?php

include './autoload.php';
include './functions.php';
include './model/Invoice.php';
include './model/InvoiceItem.php';
include './components/ExportCSV.php';

$db           = new Db();
$invoices     = new Invoice($db);
$invoiceItems = new InvoiceItem($db);

$page       = !empty($_GET['p']) ? (int)$_GET['p'] : 1;
$exportType = !empty($_GET['t']) ? $_GET['t'] : 'cr';
$invId      = $exportType === 'cp' && !empty($_GET['id']) ? $_GET['id'] : null;
$table      = empty($invId) ? $invoices : $invoiceItems;

$export = new ExportCSV($exportType);
$export->prepareData($table, $page, $invId);
$export->run();

?>