<?php

include './autoload.php';
include './functions.php';
include './model/Invoice.php';
include './model/InvoiceItem.php';
include './components/HandleStatus.php';

$db           = new Db();
$invoices     = new Invoice($db);

$itemId      =  !empty($_GET['id']) ? $_GET['id'] : null;
$status      =  isset($_GET['c']) && $_GET['c'] === "1" ? "paid" : ($_GET['c'] === "0" ? "unpaid" : null);

$result = (new HandleStatus($itemId, $status))->change(new Invoice($db));

header('Location: ' . $_SERVER['HTTP_REFERER']); //TODO: Yup, could be done with ajax and no reload, nicely.

//echo json_encode($result);
//die();

